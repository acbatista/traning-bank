import { Component } from '@angular/core';
import {MatDialog} from '@angular/material';


constructor(public dialog: MatDialog) {}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'traning-bank';
}
